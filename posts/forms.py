from django import forms
from django.forms import ModelForm
from .models import Comment, Post

class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'name',
            'release_year',
            'poster_url',
            'datepost',
            'synopsis',
            'distribut',
        ]
        labels = {
            'name': 'Título',
            'release_year': 'Data de Lançamento',
            'poster_url': 'URL do Poster',
            'datepost' : 'Atualizado em',
            'synopsis' : 'Sinopse',
            'distribut': "Distribuiçao",
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'date_post',
            'text',
        ]
        labels = {
            'author': 'Usuário',
            'date_post' : 'Atualizado em',
            'text' : 'Resenha',
        }
