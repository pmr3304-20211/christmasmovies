from django.db import models
from django.conf import settings

from datetime import datetime
from django.utils.timezone import now

class Post(models.Model):
    name = models.CharField(max_length=200)
    datepost = models.DateTimeField(default=now, editable=True)
    release_year = models.IntegerField()
    poster_url = models.URLField(max_length=200, null=True)
    synopsis = models.CharField(max_length=3000)
    distribut = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.name} ({self.release_year})'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    date_post = models.DateTimeField(default=now, editable=True)                         
    text = models.CharField(max_length=1000)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

class Category(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    posts = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.name} by {self.author}'