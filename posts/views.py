from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views import generic
from .models import Category, Post, Comment, Category
from .forms import PostForm, CommentForm



class PostListView(generic.ListView):
    model = Post
    template_name = 'posts/index.html'

class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'posts/detail.html'

def search_posts(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        post_list = Post.objects.filter(name__icontains=search_term)
        context = {'post_list': post_list}
    return render(request, 'posts/search.html', context)


class PostCreateView(generic.CreateView):
    model = Post
    template_name = 'posts/create.html'
    fields = ['name', 'release_year', 'poster_url', 'datepost', 'synopsis', 'distribut']
    def get_success_url ( self ) :
        return reverse ('posts:detail', args =(self.object.pk, ) )

class PostUpdateView(generic.UpdateView):
    model = Post
    template_name = 'posts/update.html'
    fields = ['name', 'release_year', 'poster_url', 'datepost', 'synopsis', 'distribut']
    def get_success_url ( self ) :
        return reverse ('posts:detail', args =( self.object.pk, ) )

class PostDeleteView(generic.DeleteView):
    model = Post
    template_name = 'posts/delete.html'
    def get_success_url (self) :
        return reverse ('posts:index')

def create_comment( request , post_id ) :
    post = get_object_or_404 ( Post , pk = post_id )
    if request . method == 'POST':
        form = CommentForm ( request . POST )
        if form . is_valid() :
            comment_author = form . cleaned_data ['author']
            comment_date_post = form . cleaned_data ['date_post']
            comment_text = form . cleaned_data ['text']

            comment = Comment( author = comment_author, date_post = comment_date_post, 
            text = comment_text, post = post)

            comment.save()
            return HttpResponseRedirect (reverse ('posts:detail', args =( post_id , )))
    else :
        form = CommentForm ()
    context = {'form': form , 'post': post }
    return render (request , 'posts/comment.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'posts/category.html'

class CategoryDetailView(generic.DetailView):
    model = Category
    template_name = 'posts/detail-category.html'
